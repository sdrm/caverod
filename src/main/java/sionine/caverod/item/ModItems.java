package sionine.caverod.item;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems
{
    @GameRegistry.ObjectHolder("caverod:cave_rod")
    public static ItemCaveRod itemCaveRod;

    public static void registerModels() {
        // Normal cave rod
        ModelResourceLocation normalCaveRodResLoc = new ModelResourceLocation(
                "caverod:cave_rod_normal", "inventory");
        ModelLoader.setCustomModelResourceLocation(
                ModItems.itemCaveRod, ItemCaveRod.STATE_NORMAL, normalCaveRodResLoc);

        // Glowing cave rod
        ModelResourceLocation glowingCaveRodResLoc = new ModelResourceLocation(
                "caverod:cave_rod_glowing", "inventory");
        ModelLoader.setCustomModelResourceLocation(
                ModItems.itemCaveRod, ItemCaveRod.STATE_GLOWING, glowingCaveRodResLoc);
    }
}
