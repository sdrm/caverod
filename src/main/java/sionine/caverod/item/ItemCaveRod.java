package sionine.caverod.item;

import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import sionine.caverod.config.Config;

public class ItemCaveRod extends Item
{
    public static final int STATE_NORMAL = 0;
    public static final int STATE_GLOWING = 1;
    public static double range = 0;

    public ItemCaveRod() {
        setRegistryName("cave_rod");
        setUnlocalizedName("caverod.cave_rod");

        setCreativeTab(CreativeTabs.TOOLS);
        setHasSubtypes(true);
        setMaxStackSize(1);
        setMaxDamage(0);
    }

    @Override
    public void onUpdate(ItemStack stack, World world, Entity player, int itemSlot, boolean isSelected) {
        if (!world.isRemote && range != 0) {
            // Check if in hot bar
            if (itemSlot < 0 || itemSlot > 8) return;

            // Check for cave and update meta accordingly
            int newState = testForCave(world, player) ? STATE_GLOWING : STATE_NORMAL;
            int currentState = stack.getItemDamage();
            if (currentState != newState) {
                stack.setItemDamage(newState);
            }
        }
    }

    private boolean testForCave(World world, Entity player) {
        float f = player.rotationPitch;
        float f1 = player.rotationYaw;
        double d00 = player.posX;
        double d01 = player.posY + (double) player.getEyeHeight();
        double d02 = player.posZ;
        Vec3d playerEyes = new Vec3d(d00, d01, d02);
        float f2 = MathHelper.cos(-f1 * 0.017453292F - (float) Math.PI);
        float f3 = MathHelper.sin(-f1 * 0.017453292F - (float) Math.PI);
        float f4 = -MathHelper.cos(-f * 0.017453292F);
        float f5 = MathHelper.sin(-f * 0.017453292F);
        float f6 = f3 * f4;
        float f7 = f2 * f4;

        Vec3d vec32 = playerEyes.addVector((double) f6 * range, (double) f5 * range, (double) f7 * range);

        return rayTrace(world, player.getLookVec(), playerEyes, playerEyes, vec32, 0);
    }

    private boolean rayTrace(World world, Vec3d look, Vec3d eyes, Vec3d src, Vec3d dst, int recursionDepth) {
        // Check and increment recursion depth, stop if gone too deep
        if (recursionDepth++ > 3) return false;

        double precision = Config.precision;
        int minThickness = Config.minThickness + 1;
        int minPassableBlocks = Config.minPassableBlocks;

        double effectiveRange = Math.ceil(range + 0.5);
        int passableBlocks = 0;

        RayTraceResult rtr = world.rayTraceBlocks(
                src, dst, false, true, false);

        if (rtr != null && rtr.typeOfHit == RayTraceResult.Type.BLOCK) {
            Vec3d startPoint = rtr.hitVec;

            // Loop over any block in the same line behind the "wall" block
            for (double i = 1.0; i < effectiveRange; i += precision) {
                // Calculate the point of the block to test on
                Vec3d testPoint = startPoint.add(look.scale(i));

                // Stop if further away from player than `range`
                if (testPoint.squareDistanceTo(eyes) > effectiveRange * effectiveRange) {
                    return false;
                }

                // Check whether block coordinates are valid
                BlockPos blockPos = new BlockPos(testPoint);
                if (!world.isValid(blockPos)) {
                    return false;
                }

                // Get the block in question
                IBlockState blockState = world.getBlockState(blockPos);

                // Test if passable and above threshold
                boolean isPassable = blockState.getBlock().isPassable(world, blockPos);
                if (isPassable) {
                    if (i >= minThickness) {
                        passableBlocks++;

                        if (passableBlocks >= minPassableBlocks) {
                            return true;
                        }
                    } else {
                        return rayTrace(world, look, eyes, testPoint, dst, recursionDepth);
                    }
                } else {
                    passableBlocks = 0;
                }
            }
        }

        return false;
    }
}
