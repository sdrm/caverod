package sionine.caverod.config;

import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;
import sionine.caverod.CaveRod;
import sionine.caverod.proxy.CommonProxy;

public class Config
{
    private static String CATEGORY_GENERAL = "general";

    public static double range = 12.0;
    public static double precision = 1.0;
    public static int minThickness = 1;
    public static int minPassableBlocks = 2;

    public static void readConfig() {
        Configuration config = CommonProxy.config;

        try {
            config.load();
            initGeneralConfig(config);
        } catch (Exception e) {
            CaveRod.logger.log(Level.ERROR, "Problem loading the config file", e);
        } finally {
            if (config.hasChanged()) {
                config.save();
            }
        }
    }

    private static void initGeneralConfig(Configuration config) {
        config.addCustomCategoryComment(CATEGORY_GENERAL, "General configuration");

        range = config.getFloat(
                "range",
                CATEGORY_GENERAL,
                (float)range,
                1.f,
                64.f,
                "How many blocks forward should the Cave Rod detect air at."
        );

        precision = config.getFloat(
                "precision",
                CATEGORY_GENERAL,
                (float)precision,
                0.f,
                2.f,
                "How precise the check for blocks is. The bigger it is, the faster it is, but less accurate."
        );

         minThickness = config.getInt(
                 "minThickness",
                 CATEGORY_GENERAL,
                 minThickness,
                 1,
                 8,
                 "Minimum thickness (in blocks) for a wall to be considered so"
         );

         minPassableBlocks = config.getInt(
                 "minPassableBlocks",
                 CATEGORY_GENERAL,
                 minPassableBlocks,
                 1,
                 8,
                 "How many empty blocks behind a wall for it to be considered a cave"
         );
    }
}
