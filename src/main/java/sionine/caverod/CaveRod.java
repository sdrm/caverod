package sionine.caverod;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;
import sionine.caverod.proxy.CommonProxy;

@Mod(modid = CaveRod.MODID, name = CaveRod.NAME, version = CaveRod.VERSION)
public class CaveRod
{
    public static final String MODID = "caverod";
    public static final String NAME = "Cave Rod";
    public static final String VERSION = "1.0";

    @SidedProxy(
            clientSide = "sionine.caverod.proxy.ClientProxy",
            serverSide = "sionine.caverod.proxy.CommonProxy"
    )
    public static CommonProxy proxy;

    @Mod.Instance
    public static CaveRod instance;

    public static Logger logger;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();

        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
}
