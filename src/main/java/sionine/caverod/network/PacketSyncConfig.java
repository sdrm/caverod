package sionine.caverod.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import sionine.caverod.config.Config;
import sionine.caverod.item.ItemCaveRod;

public class PacketSyncConfig implements IMessage
{
    private double range;

    @Override
    public void fromBytes(ByteBuf buf) {
        range = buf.readDouble();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeDouble(range);
    }

    public PacketSyncConfig() {
        range = Config.range;
    }

    public static class Handler implements IMessageHandler<PacketSyncConfig, IMessage>
    {
        @Override
        public IMessage onMessage(PacketSyncConfig message, MessageContext ctx) {
            // Run on the main thread
            FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> handle(message, ctx));
            return null;
        }

        private void handle(PacketSyncConfig message, MessageContext ctx) {
            ItemCaveRod.range = message.range;
        }
    }
}
